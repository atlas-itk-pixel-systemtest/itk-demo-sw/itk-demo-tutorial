# itk-demo-tutorial

## Getting started

This repository contains minimal examples focussing on one aspect of the microservices architecture.

For a complete example with many of the available features implemented see `itk-demo-template`.

Unlike the microservice repositories `itk-demo-*` which each contain the complete directory structure
for a single microservice, in this tutorial each subdirectory contains the complete example.

1. Flask API server
    1. flask-minimal
        1. python toolchain
        1. Flask server deployment
    1. 

1. React frontend

1. backend
    1. pybind11 example

