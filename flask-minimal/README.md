

# Minimal Flask + connexion Example

This tutorial explains how the Flask REST API server works mostly by walking you through all the files.

One idea of the microservices architecture is to keep the same file naming conventions for all microservices.

The top level directory
```shell
README.md         # this file
.python-version   # Python version used by pyenv to automatically use this 
                  # Python version when entering directory
pyproject.toml    # Python standard package information file listing module dependencies, build system requirements and meta information
.flaskenv         # used by Flask to find entrypoint function for flask command
```

The Python package
```shell
flask_minimal/    # the Python module uses the name of the
                  # repository in snake case (lower case + underscores)
    app.py        # the Flask app - see comments inside the file
    routes.py     # functions mapped to the routes defined in OpenAPI.yml
```

The OpenAPI yaml definition
```shell
openapi/
    openapi.yml   # The OpenAPI YAML file defining the REST API endpoints
```

## Installation
As a prerequisite you need Python and `poetry` installed.
  (--> link to Python / poetry setup guide)

Install the modules defined in pyproject.toml and their dependencies
```shell
poetry install

# in case poetry gets stuck, try
export PYTHON_KEYRING_BACKEND=keyring.backends.fail.Keyring
```
Activate the local venv installed in the previous step
```shell
poetry shell
```
Check if Flask and connexion work correctly
```shell
flask routes
```
This should list the endpoint defined in `openapi.yml` as well as the
endpoints for the Swagger UI.

## Flask server deployment

There are several methods to deploy the Flask server.
It makes sense to understand all of them since they are needed in different situations.

### Flask development server (local)
The first method to serve the API is to use the built-in Flask development server.
This method is only used for development / debugging, as the name says.
### gunicorn (local)
This method serves the API via `gunicorn`
It can be used in native deployments where a Docker deployment is not needed or possible.
### gunicorn (Docker)




