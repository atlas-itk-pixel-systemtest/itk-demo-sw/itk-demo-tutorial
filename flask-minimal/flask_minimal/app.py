import logging

from connexion import FlaskApp

def create_app(config=None):

    app = FlaskApp(
        __name__,
        specification_dir="../openapi/",
        options={
            "swagger_ui": True,
            "swagger_url": "/ui/",
        },
        server_args={
            "static_folder": "../ui/build",
            "static_url_path": "/",
        },
    )

    flask_app = app.app

    with flask_app.app_context():
        app.add_api("openapi.yml")

        @flask_app.route("/")
        def index():
            return flask_app.send_static_file("index.html")

    return app

def create_flask_app(config=None):
    """entry point for flask development server"""
    app = create_app(config)

    return app.app
